const isAuthorised = (req, res, next) => {
   if (
      req &&
      req.headers &&
      req.headers.authorization &&
      req.headers.authorization === 'admin'
   ) {
      next();
   } else {
      res.status(401).send(`You are not authorised!`);
   }
};

module.exports = { isAuthorised };

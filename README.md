# lecture-project

server port 3000

GET: /user
получение массива всех пользователей

GET: /user/:id
получение одного пользователя по ID

POST: /user
создание пользователя по данным передаваемым в теле запроса

PUT: /user/:id
обновление пользователя по данным передаваемым в теле запроса

DELETE: /user/:id
удаление одного пользователя по ID

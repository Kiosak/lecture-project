const { saveData } = require('../repositories/user.repository');
const fs = require('fs');

let users = JSON.parse(fs.readFileSync('userlist.json', 'utf8'));

const getUser = () => {
   return users;
};

const getUserId = id => {
   return users.find(user => user._id === id);
};

const postUser = user => {
   if (user._id) {
      return false;
   } else {
      const _id = +users[users.length - 1]._id + 1;
      user = { _id, ...user };
      users.push(user);
      return user;
   }
};

const putUser = (id, newData) => {
   if (getUserId(id)) {
      users = users.map(user => {
         if (user._id === id) {
            for (const key in newData) {
               if (newData.hasOwnProperty(key) && key !== '_id') {
                  user[key] = newData[key];
               }
            }
         }
         return user;
      });
   }
   return getUserId(id);
};

const deleteUser = id => {
   if (getUserId(id)) {
      const deletedUser = getUserId(id);
      users = users.filter(user => user._id !== id);
      return deletedUser;
   } else {
      return false;
   }
};

module.exports = {
   getUser,
   getUserId,
   postUser,
   putUser,
   deleteUser,
};

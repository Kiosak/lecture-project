var express = require('express');
var router = express.Router();

const {
   getUser,
   getUserId,
   postUser,
   putUser,
   deleteUser,
} = require('../services/user.service');
const { isAuthorised } = require('../middlewares/auth.middlewares');

router.get('/', function(req, res, next) {
   res.send(getUser());
});

router.get('/:id', function(req, res, next) {
   const user = req.params;
   const isUser = getUserId(user.id);
   if (isUser) {
      res.send(isUser);
   } else {
      res.status(404).send(`There is no such user id:${user.id}`);
   }
});

router.post('/', isAuthorised, function(req, res, next) {
   if (isAuthorised && req.body) {
      const newUser = postUser(req.body);
      if (newUser) {
         res.status(201).send(newUser);
      } else {
         res.status(403).send(`You shouldn't set user._id`);
      }
   } else {
      res.status(400).send(`You made a bad request`);
   }
});

router.put('/:id', isAuthorised, function(req, res, next) {
   const user = req.params;
   const newData = req.body;
   const userUpdated = putUser(user.id, newData);
   if (userUpdated) {
      res.send(userUpdated);
   } else {
      res.status(400).send(`You made a bad request`);
   }
});

router.delete('/:id', isAuthorised, function(req, res, next) {
   const user = req.params;
   const deletedUser = deleteUser(user.id);
   if (deletedUser) {
      res.send(deletedUser);
   } else {
      res.status(404).send(`There is no such user id:${user.id}`);
   }
});

module.exports = router;
